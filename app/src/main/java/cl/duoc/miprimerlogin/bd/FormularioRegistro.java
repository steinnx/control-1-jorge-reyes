package cl.duoc.miprimerlogin.bd;

import java.util.ArrayList;

import cl.duoc.miprimerlogin.entidades.Formulario;

/**
 * Created by DUOC on 25-03-2017.
 */

public class FormularioRegistro {
    private static ArrayList<Formulario> formularios = new ArrayList<>();

    public static void agregarFormulario(Formulario form){
        formularios.add(form);
    }

    public static ArrayList<Formulario> getFormularios(){
        return formularios;
    }
}
