package cl.duoc.miprimerlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtUsuario,edtContraseña;
    private Button btnEntrar,btnRegistro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUsuario = (EditText) findViewById(R.id.editUsuario);
        edtContraseña= (EditText) findViewById(R.id.editContraseña);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);

        btnEntrar.setOnClickListener(this);
        btnRegistro.setOnClickListener(this);

        /*btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IngresarUsuario();
            }
        });
        */
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnEntrar){

        }else if (v.getId()==R.id.btnRegistro){
            Intent i = new Intent(LoginActivity.this,RegistroActivity.class);
            startActivity(i);
        }
    }

    /*private void IngresarUsuario() {
            if (edtContraseña.getText().toString().equals("admin") &&edtUsuario.getText().toString().equals("admin")   ){
                Toast.makeText(LoginActivity.this, "Usuario Valido", Toast.LENGTH_SHORT).show();

            }else{
                Toast.makeText(LoginActivity.this, "Usuario No Valido", Toast.LENGTH_SHORT).show();
            }

    }
    */
}
